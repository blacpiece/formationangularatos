# Crm

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.1.

## Init projet

- command :
  - `npm install` ( rebuild node_modules file)
  - `ng serve` ( lance le serveur angular)
  - `json-server --watch api/db.json` ( lance le serveur api json )

## API json-serv

- command :
  - `npm install --save-dev json.server` ( permet d'insaller via npm une api json)

## Documentation de l'application : Compodoc

Utilisation de l'outil compodoc pour générer la documentation de l'application :

- `npm install --save-dev @compodoc/compodoc` (installation de l'outil, seulement dans l'environnement de dev)
- `compodoc -p tsconfig.json -o -s` (génération de la documentation [+ -s +] permet de lancer un serveur pour avoir la document, sinon la documentation est fournie dans le dossier `documentation` à la racine du projet)

## Library Dayjs ( surtout pas Moment : beaucoup trop lourd ( elle importe toutes les langues contrairement à Dayjs ))

Utilisation de la librery Dayjs

- command :
  - `npm install dayjs` ( install dayjs )

## Library webpack

Utilisation de la library webpack pour analyzer les fichiers de build.

- command :
  - `npm install webpack-bundle-analyzer --save-dev` ( installation de la library )
  - `ng build --prod --stats-json && webpack-bundle-analyzer dist/crm/stats.json` ( lancement de l'analyse )

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
Utiliser `ng build --prod` pour effectuer un build pour le serveur de prod.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## identification des blocks

### Modules

- clients
- orders ( rajouter le flag  orders --routing )
- login ( rajouter le flag  login --routing )
- shared
- core
- ui

[- !! Pensez à rattacher les modules créer au AppModule -]

### Components

#### dans le module login

- dossier pages :
  - login

### dans le module clients

- dossier pages:
  - clientAdd
  - clientEdit
  - clientList

#### dans le module orders

- dossier pages:
  - orderAdd
  - orderEdit
  - orderList

#### dans le module shared

- dossier components :
  - bandeau
  - button
  - tableau
  - select
  - button-action

#### dans le module ui

- dossier container:
  - ui
- dossier components :
  - header
  - nav
  - footer
