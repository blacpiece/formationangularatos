import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { filter, map } from 'rxjs/operators';
import { ClientState } from 'src/app/shared/enums/client-state.enum';
import { Client } from 'src/app/shared/insterfaces/client';

/**
 * FormControl => 1 champ de notre formulaire
 * FormGroup => 1 formulaire ( cad un groupe de champ )
 * Service FormBuilder => 1 service pour créer facilement FormGroup/FormControl
 */

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.scss']
})
export class ClientFormComponent implements OnInit {

  public myForm!:FormGroup
  @Output()
  public submitted = new EventEmitter
  @Input()
  public clientInitial:Client = {
    name:'',
    email:'',
    comment:'',
    state: ClientState.ACTIF
  }
  public clientStatus = Object.values(ClientState)

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.myForm = this.formBuilder.group({
      name : [this.clientInitial.name,Validators.required],
      email : this.formBuilder.control(this.clientInitial.email,Validators.compose([Validators.required,Validators.email])),
      comment: this.clientInitial.comment,
      state:this.clientInitial.state
    })

    this.name.valueChanges.pipe(
      filter(val => val === "boon")
    ).subscribe(
      (data)=>{
        this.email.setValue("boon@boon.com")
      }
    )
  }

  get name(){
    return this.myForm.get('name') as FormControl
  }

  get email(){
    return this.myForm.get('email') as FormControl
  }

  get comment(){
    return this.myForm.get('comment') as FormControl
  }

  register(){
    this.submitted.emit(this.myForm.value)
  }

}
