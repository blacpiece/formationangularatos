import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Client } from 'src/app/shared/insterfaces/client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private baseURL = `${environment.urlApi}/clients`;

  constructor(private http: HttpClient) { }

  list(){
    return this.http.get<Client[]>(this.baseURL)
  }

  get(id:string){
    return this.http.get<Client>(`${this.baseURL}/${id}`)
  }

  add(data:Client){
    return this.http.post(this.baseURL,data)
  }

  update(data:Client){
    return this.http.put(`${this.baseURL}/${data.id}`,data)
  }

  delete(data:Client){
    return this.http.delete(`${this.baseURL}/${data.id}`)
  }
}
