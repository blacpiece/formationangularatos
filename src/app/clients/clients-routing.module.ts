import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from '../core/guards/admin.guard';
import { ConnectedGuard } from '../core/guards/connected.guard';
import { PageNotFoundComponent } from '../page-not-found/pages/page-not-found/page-not-found.component';
import { ClientAddComponent } from './pages/client-add/client-add.component';
import { ClientCommentComponent } from './pages/client-comment/client-comment.component';
import { ClientEditComponent } from './pages/client-edit/client-edit.component';
import { ClientListComponent } from './pages/client-list/client-list.component';

const routes: Routes = [
  { path:'list', component: ClientListComponent,canActivate:[ConnectedGuard],children: [
    { path: 'comment', component: ClientCommentComponent}
  ] },
  { path:'add', component: ClientAddComponent, canActivate:[AdminGuard,ConnectedGuard] },
  { path:'edit/:id', component: ClientEditComponent,canActivate:[ConnectedGuard] },
  { path:'',component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
