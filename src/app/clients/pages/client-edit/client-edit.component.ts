import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from 'src/app/shared/insterfaces/client';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss']
})
export class ClientEditComponent implements OnInit {

  public client!:Client

  constructor(private router:Router,private route:ActivatedRoute,private clientService:ClientService) { }

  ngOnInit(): void {
    //subscribe hot
    this.route.paramMap.subscribe(
      (param) => {
        const id = param.get('id')
        if(id){
          //subscribe cold
          this.clientService.get(id).subscribe(
            (data) => this.client = data
          )
        }
      }
    )
  }

  editClient(data:Client){
    const item = { ...data, id:this.client.id}
    this.clientService.update(item).subscribe(
      ()=> this.router.navigate(['/clients','list'])
    )
  }

  removeClient(){
    this.clientService.delete(this.client).subscribe(
      ()=> this.router.navigate(['/clients','list'])
    )
  }

}
