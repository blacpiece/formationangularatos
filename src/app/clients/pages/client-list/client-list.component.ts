import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientState } from 'src/app/shared/enums/client-state.enum';
import { Client } from 'src/app/shared/insterfaces/client';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {

  public heardersClients = ['name', 'email','comment', 'state','id','actions'];
  public clients:Client[] = []
  public clientStatus = Object.values(ClientState)

  constructor(private router: Router,private clientService: ClientService) {
    this.clientService.list().subscribe(
      (data)=> {
        this.clients = data
      }
    )
  }

  ngOnInit(): void {
  }

  goToAdd(){
    // rediriger l'utilisateur vers la page /clients/add
    this.router.navigate(['/clients','add']);
  }

  modifStatusClient(client:Client){
    this.clientService.update(client).subscribe()
  }

  removeClient(client:Client){
    this.clientService.delete(client).subscribe(
      ()=>{
        this.clientService.list().subscribe(
          (data)=> {
            this.clients = data
          }
        )
      }
    )

  }

}
