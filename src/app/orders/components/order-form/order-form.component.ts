import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClientService } from 'src/app/clients/services/client.service';
import { OrderState } from 'src/app/shared/enums/order-state.enum';
import { OrderType } from 'src/app/shared/enums/order-type.enum';
import { Client } from 'src/app/shared/insterfaces/client';
import { Order } from 'src/app/shared/insterfaces/order';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss']
})
export class OrderFormComponent implements OnInit {

  @Output()
  public eventSub = new EventEmitter

  @Input()
  public initialOrder:Order = {
    clientId:1,
    duration:0,
    comment:'',
    state:OrderState.ON_GOING,
    type:OrderType.FORMATION,
    tjmHt:0,
    tva:0
  }
  @Input()
  public buttonSubmitText = "Ajouter un order"

  public orderForm!:FormGroup
  public clientList!:Client[]
  public orderStates = Object.values(OrderState)
  public orderTypes = Object.values(OrderType)

  constructor(private formBuilder : FormBuilder,private clientService: ClientService) {
    clientService.list().subscribe(
      (clients) => this.clientList = clients
    )
  }

  ngOnInit(): void {
    this.orderForm = this.formBuilder.group({
      clientId: this.initialOrder.clientId,
      duration: [this.initialOrder.duration,[Validators.required,Validators.min(1)]],
      comment:this.initialOrder.comment,
      state:this.initialOrder.state,
      type:this.initialOrder.type,
      tjmHt:[this.initialOrder.tjmHt,[Validators.required,Validators.min(1)]],
      tva: [this.initialOrder.tva,[Validators.required,Validators.min(1)]]
    })
  }

  onSubmit(){
    this.eventSub.emit(this.orderForm.value)
  }

  get duration(){
    return this.orderForm.get('duration') as FormControl
  }

  get tjmHt(){
    return this.orderForm.get('tjmHt') as FormControl
  }

  get tva(){
    return this.orderForm.get('tva') as FormControl
  }

  test(t:any){

  }



}
