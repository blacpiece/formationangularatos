import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/shared/insterfaces/order';
import { OrderService } from '../../services/order.service';

@Component({
  selector: 'app-order-edit',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderEditComponent implements OnInit {

  public order!:Order

  constructor(private router:Router,private route:ActivatedRoute,private orderService:OrderService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (param)=>{
        const id = param.get('id')
        if(id){
          this.orderService.get(id).subscribe(
            (data) => {
              this.order = data
            }
          )
        }
      }
    )
  }

  editOrder(submitted:Order){
    submitted.id = this.order.id
    this.orderService.update(submitted).subscribe(
      ()=> this.router.navigate(['/orders','list'])
    )
  }

  deletOrder(){
    this.orderService.delete(this.order).subscribe(
      ()=>this.router.navigate(['/orders','list'])
    )
  }

}
