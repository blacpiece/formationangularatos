import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Order } from 'src/app/shared/insterfaces/order';
import { OrderService } from '../../services/order.service';

@Component({
  selector: 'app-order-add',
  templateUrl: './order-add.component.html',
  styleUrls: ['./order-add.component.scss']
})
export class OrderAddComponent implements OnInit {

  constructor(private orderService:OrderService,private router: Router) { }

  ngOnInit(): void {
  }

  addOrder(order:Order){
    this.orderService.add(order).subscribe(
      () => this.router.navigate(['/orders','list'])
    )
  }

}
