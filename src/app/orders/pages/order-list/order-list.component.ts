import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientService } from 'src/app/clients/services/client.service';
import { OrderState } from 'src/app/shared/enums/order-state.enum';
import { Client } from 'src/app/shared/insterfaces/client';
import { Order } from 'src/app/shared/insterfaces/order';
import { OrderService } from '../../services/order.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  public heardersOrders = [ 'ID','Client','Durée','TVA','TJM HT','Commentaire','Type','State','Option'];
  public orders!:Order[]
  public clients!:Client[]
  public orderStates = Object.values(OrderState)

  constructor(private router:Router, private orderService: OrderService,private clientService:ClientService) {
    clientService.list().subscribe(
      (data)=> {
        this.clients = data
      }
    )

    orderService.list().subscribe(
      (data) => {
        this.orders = data
      }
    )
  }

  ngOnInit(): void {
  }

  deletOrder(order:Order){
    this.orderService.delete(order).subscribe(
      ()=> {
        this.orderService.list().subscribe(
          (data) => {
            this.orders = data
          }
        )
      }
    )
  }

  updateOrder(order:Order){
    this.orderService.update(order).subscribe(
      ()=> {
        this.orderService.list().subscribe(
          (data) => {
            this.orders = data
          }
        )
      }
    )
  }

  goToAdd(){
    // rediriger vers add to order
    this.router.navigate(['/orders','add']);
  }

}
