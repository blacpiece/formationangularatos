import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrderAddComponent } from './pages/order-add/order-add.component';
import { OrderEditComponent } from './pages/order-edit/order-edit.component';
import { OrderListComponent } from './pages/order-list/order-list.component';
import { SharedModule } from '../shared/shared.module';
import { OrderFormComponent } from './components/order-form/order-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BootstrapIconsModule } from 'ng-bootstrap-icons';
import { allIcons } from 'ng-bootstrap-icons/icons';


@NgModule({
  declarations: [OrderAddComponent, OrderEditComponent, OrderListComponent, OrderFormComponent],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    BootstrapIconsModule.pick(allIcons)
  ]
})
export class OrdersModule { }
