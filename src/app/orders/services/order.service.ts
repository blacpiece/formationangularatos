import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from 'src/app/shared/insterfaces/order';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  list(){
    return this.http.get<Order[]>(`${environment.urlApi}/orders`)
  }

  get(id:string){
    return this.http.get<Order>(`${environment.urlApi}/orders/${id}`)
  }

  add(order:Order){
    return this.http.post(`${environment.urlApi}/orders/`,order)
  }

  update(order:Order){
    return this.http.put(`${environment.urlApi}/orders/${order.id}`,order)
  }

  delete(order:Order){
    return this.http.delete(`${environment.urlApi}/orders/${order.id}`)
  }
}
