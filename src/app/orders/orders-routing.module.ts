import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnectedGuard } from '../core/guards/connected.guard';
import { PageNotFoundComponent } from '../page-not-found/pages/page-not-found/page-not-found.component';
import { OrderAddComponent } from './pages/order-add/order-add.component';
import { OrderEditComponent } from './pages/order-edit/order-edit.component';
import { OrderListComponent } from './pages/order-list/order-list.component';

const routes: Routes = [
  {path:'list', component: OrderListComponent,canActivate:[ConnectedGuard]},
  {path:'add', component: OrderAddComponent,canActivate:[ConnectedGuard]},
  {path:'edit/:id', component: OrderEditComponent,canActivate:[ConnectedGuard]},
  {path:'', component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
