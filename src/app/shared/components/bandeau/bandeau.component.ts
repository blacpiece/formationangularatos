import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bandeau',
  templateUrl: './bandeau.component.html',
  styleUrls: ['./bandeau.component.scss']
})
export class BandeauComponent implements OnInit {

  @Input()
  public titleText!: string;
  @Input()
  public leadText!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
