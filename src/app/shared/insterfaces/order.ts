import { OrderState } from "../enums/order-state.enum";
import { OrderType } from "../enums/order-type.enum";

export interface Order{
  id?:number;
  clientId:number;
  duration:number;
  tva:number;
  tjmHt:number;
  comment:string;
  type:OrderType;
  state:OrderState;
}
