export enum OrderState {
  ON_GOING = "En cours",
  ENDED = "Fini"
}
