import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VersionService {
  // $ convention pour définir un flux
  private versionP$ = new BehaviorSubject(42)
  public version$ = this.versionP$.asObservable().pipe(
    map( x => x*2)
  )


  constructor() { }

  increment(){
    this.versionP$.next(this.versionP$.value+1)
    console.log(this.versionP$.value)
  }

  decrement(){
    this.versionP$.next(this.versionP$.value-1)
    console.log(this.versionP$.value)
  }
}
