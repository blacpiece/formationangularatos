import { Injectable, OnDestroy } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { LoginServiceService } from 'src/app/login/services/login-service.service';

@Injectable({
  providedIn: 'root'
})
export class ConnectedGuard implements CanActivate,OnDestroy {

  private con$!:Subscription
  private con!:boolean

  constructor(private loginService:LoginServiceService) {
    this.con$ = loginService.connect$.subscribe(
      (data) => this.con = data
    )
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.con;
  }

  ngOnDestroy(){
    this.con$.unsubscribe()
  }

}
