import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private menuOpenP$ = new BehaviorSubject(true)
  public menuOpen$ = this.menuOpenP$.asObservable();

  constructor() { }

  toggle(){
    this.menuOpenP$.next(!this.menuOpenP$.value)
  }
}
