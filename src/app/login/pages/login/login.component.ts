import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MenuService } from 'src/app/core/menu.service';
import { VersionService } from 'src/app/core/version.service';
import { LoginServiceService } from '../../services/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public openMenuT!:string
  private sub$: Subscription
  private con$: Subscription
  public con!:boolean
  public user = {
    userName: 'test',
    password: 'test'
  }

  constructor(private versionService: VersionService,private menuService: MenuService,private loginService:LoginServiceService) {
    this.sub$ = menuService.menuOpen$.subscribe(
      (data) => this.openMenuT = data ? "ouvert" : "fermé"
    )

    this.con$ = loginService.connect$.subscribe(
      (data)=> this.con = data
    )
  }

  ngOnInit(): void {
  }

  public clickIncrement(){
    this.versionService.increment()
  }

  public clickDecrement(){
    this.versionService.decrement()
  }

  toggleMenu(){
    this.menuService.toggle()
  }

  ngOnDestroy(){
    this.sub$.unsubscribe()
    this.con$.unsubscribe()
  }

  login(){
    this.loginService.login()
  }

  logout(){
    this.loginService.logout()
  }

}
