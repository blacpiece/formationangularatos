import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  private con:boolean = false;
  private connectP$ = new BehaviorSubject(this.con)
  public connect$ = this.connectP$.asObservable()

  constructor() {
    if(localStorage.getItem('connect')){
      this.con = localStorage.getItem('connect') == "true"
      this.connectP$.next(this.con)
    }
    else{
      localStorage.setItem('connect',""+this.con)
    }
  }

  login(){
    this.con = true
    localStorage.setItem('connect',""+this.con)
    this.connectP$.next(this.con)
  }

  logout(){
    this.con = false
    localStorage.setItem('connect',""+this.con)
    this.connectP$.next(this.con)
  }
}
