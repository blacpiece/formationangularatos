import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { LoginServiceService } from 'src/app/login/services/login-service.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public con$!:Observable<boolean>

  constructor(private loginService:LoginServiceService) {
    this.con$ = loginService.connect$
  }

  ngOnInit(): void {
  }

}
