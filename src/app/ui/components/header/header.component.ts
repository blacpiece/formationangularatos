import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { VersionService } from 'src/app/core/version.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public version$: Observable<number>;

  // du moment ou l'on met un pipe : il faut changer BehaviorSubject<number> en Observable<number>
  constructor(private versionService: VersionService) {
    this.version$ = this.versionService.version$.pipe(
      filter(x => x%2==0)
    )
  }

  ngOnInit(): void {
  }

}
