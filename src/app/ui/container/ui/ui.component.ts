import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/core/menu.service';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.scss']
})
export class UiComponent implements OnInit {

  public openMenuT!: string
  public openMenuB!: boolean

  constructor(private menuService: MenuService) {
    menuService.menuOpen$.subscribe(
      (data)=>{
        this.openMenuB = data;
        this.openMenuT = data ? "ouvert" : "fermé"
      }
    )
  }

  ngOnInit(): void {
  }

  toggle(){
    this.menuService.toggle()
  }
}
