import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

//Mise en prod
if (environment.production) {
  enableProdMode();
}
//démarrage de l'application par le module AppModule
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
